/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.hint.spo.stn.LoginExt;

import com.smartfoxserver.v2.components.login.LoginAssistantComponent;
import com.smartfoxserver.v2.extensions.SFSExtension;
import java.util.Properties;

/**
 *
 * @author stn
 */
public class ZoneExtension extends SFSExtension {
    private LoginAssistantComponent lac;
    
    @Override
    public void init () {
        String loginTable = "users"; // Default: users
        String userNameField = "username"; // Default: username
        String passwordField = "password"; // Default: password
        
        Properties props = getConfigProperties();
        if (props != null) {
            loginTable = props.getProperty("loginTable", loginTable);
            userNameField = props.getProperty("userNameField", userNameField);
            passwordField = props.getProperty("passwordField", passwordField);
        }
        
        lac = new LoginAssistantComponent(this);
        lac.getConfig().loginTable = loginTable; // Default: users
        lac.getConfig().userNameField = userNameField; // Default: username
        lac.getConfig().passwordField = passwordField; // Default: password
        
    }
    
    @Override
    public void destroy () {
        super.destroy();
    }
}

