/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.hint.spo.stn.SignupExt;

import com.smartfoxserver.v2.components.signup.PasswordMode;
import com.smartfoxserver.v2.components.signup.RecoveryMode;
import com.smartfoxserver.v2.components.signup.SignUpAssistantComponent;
import com.smartfoxserver.v2.extensions.SFSExtension;
import java.util.Arrays;
import java.util.Properties;

/**
 *
 * @author stn
 */
public class ZoneExtension extends SFSExtension {
    private SignUpAssistantComponent suac;
    
    @Override
    public void init () {
        String signUpTable = "users"; // Default: users
        String idField = "id"; // Default: id
        String userNameField = "username"; // Default: username
        String passwordField = "password"; // Default: password
        String passwordMode = "PLAIN_TEXT"; // Default: PLAIN_TEXT
        String emailField = "email"; // Default: email
        String emailResponseIsActive = "false"; // Default: false
        String emailResponseFromAddress = "smartfox@mediastudent.no";
        String emailResponseSubject = "Thanks for signing up!";
        String emailResponseTemplate = "SignUpEmailTemplates/SignUpConfirmation.html";
        String passwordRecovery = "false";
        String recoveryMode = "GENERATE_NEW"; // GENERATE_NEW|SEND_OLD
        String emailRecoverySubject = "Password recovery service";
        String emailRecoveryTemplate = "SignUpEmailTemplates/PasswordRecovery.html";
        
        Properties props = getConfigProperties();
        if (props != null) {
            idField = props.getProperty("idField", idField);
            signUpTable = props.getProperty("loginTable", signUpTable);
            userNameField = props.getProperty("userNameField", userNameField);
            passwordField = props.getProperty("passwordField", passwordField);
            passwordMode = props.getProperty("passwordMode", passwordMode);
            emailField = props.getProperty("emailField", emailField);
            emailResponseIsActive = props.getProperty("emailResponseIsActive", emailResponseIsActive);
            emailResponseFromAddress = props.getProperty("emailResponseFromAddress", emailResponseFromAddress);
            emailResponseSubject = props.getProperty("emailResponseSubject", emailResponseSubject);
            emailResponseTemplate = props.getProperty("emailResponseTemplate", emailResponseTemplate);
            passwordRecovery = props.getProperty("passwordRecovery", passwordRecovery);
            recoveryMode = props.getProperty("recoveryMode", recoveryMode);
            emailRecoverySubject = props.getProperty("emailRecoverySubject", emailRecoverySubject);
            emailRecoveryTemplate = props.getProperty("emailRecoveryTemplate", emailRecoveryTemplate);
        }

        // User DB setup:
        suac = new SignUpAssistantComponent();
        suac.getConfig().signUpTable = signUpTable; // Default: users
        suac.getConfig().idField = idField; // Default: id
        suac.getConfig().usernameField = userNameField; // Default: username
        suac.getConfig().passwordField = passwordField; // Default: password
        if (passwordMode.equals("PLAIN_TEXT")) suac.getConfig().passwordMode = PasswordMode.PLAIN_TEXT;
        else if (passwordMode.equals("MD5")) suac.getConfig().passwordMode = PasswordMode.MD5;
        suac.getConfig().emailField = emailField; // Default: email
        suac.getConfig().extraFields = null; // Default: null
        suac.getConfig().extraFields = Arrays.asList("data");
        
        // Email response setup:
        if (emailResponseIsActive.equals("true")) suac.getConfig().emailResponse.isActive = true;
        else if (emailResponseIsActive.equals("false")) suac.getConfig().emailResponse.isActive = false;
        suac.getConfig().emailResponse.fromAddress = emailResponseFromAddress;
        suac.getConfig().emailResponse.subject = emailResponseSubject;
        suac.getConfig().emailResponse.template = emailResponseTemplate;
        
        // Password recovery setup:
        if (passwordRecovery.equals("true")) suac.getConfig().passwordRecovery.isActive = true;
        else if (passwordRecovery.equals("false")) suac.getConfig().passwordRecovery.isActive = false;
        if (recoveryMode.equals("GENERATE_NEW")) suac.getConfig().passwordRecovery.mode = RecoveryMode.GENERATE_NEW;
        else if (recoveryMode.equals("SEND_OLD")) suac.getConfig().passwordRecovery.mode = RecoveryMode.SEND_OLD;
        suac.getConfig().passwordRecovery.email.fromAddress = emailResponseFromAddress;
        suac.getConfig().passwordRecovery.email.subject = emailRecoverySubject;
        suac.getConfig().passwordRecovery.email.template = emailRecoveryTemplate;
        
        addRequestHandler(SignUpAssistantComponent.COMMAND_PREFIX, suac);
    }
    
    @Override
    public void destroy () {
        super.destroy ();
    }
    
}
