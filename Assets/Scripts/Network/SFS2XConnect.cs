using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Sfs2X;
using Sfs2X.Core;
using Sfs2X.Requests;
using Sfs2X.Entities;
using Sfs2X.Util;

public class SFS2XConnect : MonoBehaviour {
	
	public string version = "";
	public string ConfigFile = "Scripts/Network/sfs-config.xml";
	public bool UseConfigFile = false;
	public string ServerIP = "smartfox1.hint.no";
	public int ServerPort = 9933;
	public string ZoneName = "BasicExamples";
	public string UserName = "";
	public string Password = "";
	public string RoomName = "The Lobby";
	public bool isWebPlayerVersion = false;
	public Text debugText;
	public bool addToDebugLog = false;
	private uint debugNum = 0;

	private SmartFox sfs;

	// Use this for initialization
	void Start () {
		debugText.text = "SPO SmartFox-test2 - version " + version;

		if (isWebPlayerVersion) {
			DebugAdd ("is WebPlayer Version");
			if (!Security.PrefetchSocketPolicy(ServerIP, ServerPort, 500)) {
				DebugAdd ("Security Exception. Policy file loading failed!");
			}
		}

		sfs = new SmartFox();
		sfs.ThreadSafeMode = true;
		
		sfs.AddEventListener (SFSEvent.CONNECTION, OnConnection);
		sfs.AddEventListener (SFSEvent.LOGIN, OnLogin);
		sfs.AddEventListener (SFSEvent.LOGIN_ERROR, OnLoginError);
		sfs.AddEventListener (SFSEvent.CONFIG_LOAD_SUCCESS, OnConfigLoad);
		sfs.AddEventListener (SFSEvent.CONFIG_LOAD_FAILURE, OnConfigFail);
		sfs.AddEventListener (SFSEvent.ROOM_JOIN, OnJoinRoom);
		sfs.AddEventListener (SFSEvent.ROOM_JOIN_ERROR, OnJoinRoomError);
		sfs.AddEventListener (SFSEvent.PUBLIC_MESSAGE, OnPublicMessage);
		
		if (UseConfigFile) {
			DebugAdd ("Connecting using config file: " + ConfigFile);
			sfs.LoadConfig(Application.dataPath + "/" + ConfigFile);
		} else {		
			DebugAdd ("Connecting to " + ServerIP + ":" + ServerPort);
			sfs.Connect(ServerIP, ServerPort);
		}
	}
	
	void OnConfigFail (BaseEvent e) {
		DebugAdd ("Failed to load Config File");
	}
	
	void OnConfigLoad (BaseEvent e) {
		DebugAdd ("Config File Loaded");
		sfs.Connect (sfs.Config.Host, sfs.Config.Port);
	}
	
	void OnLogin (BaseEvent e) {
		DebugAdd ("Logged In: " + e.Params["user"]);
		sfs.Send (new JoinRoomRequest(RoomName));
	}
	
	void OnJoinRoom (BaseEvent e) {
		DebugAdd ("Joined Room: " + e.Params["room"]);
		sfs.Send (new PublicMessageRequest("Hello World!"));
	}
	
	void OnPublicMessage (BaseEvent e) {
		Room room = (Room) e.Params["room"];
		User sender = (User) e.Params["sender"];
		DebugAdd ("[" + room.Name + "] " + sender.Name + ": " + e.Params["message"]);
	}
	
	void OnJoinRoomError (BaseEvent e) {
		DebugAdd ("JoinRoom Error (" + e.Params["errorCode"] + "): " + e.Params["errorMessage"]);
	}
	
	void OnLoginError (BaseEvent e) {
		DebugAdd ("Login error: (" + e.Params["errorCode"] + "): " + e.Params["errorMessage"]);
	}
	
	void OnConnection (BaseEvent e) {
		if ((bool) e.Params["success"]) {
			if (UseConfigFile) {
				DebugAdd ("Successfully Connected to " + sfs.Config.Host + ":" + sfs.Config.Port);
			} else {
				DebugAdd ("Successfully Connected to " + ServerIP + ":" + ServerPort);
			}
			if (UseConfigFile) {
				ZoneName = sfs.Config.Zone;
			}
			//sfs.Send (new LoginRequest(UserName, Password, ZoneName));
			//sfs.Send (new LoginRequest(UserName, MD5.Md5Sum(Password), ZoneName));
			sfs.Send (new LoginRequest(UserName, PasswordUtil.MD5Password(Password), ZoneName));
		} else {
			DebugAdd ("Connection Failed to " + ServerIP + ":" + ServerPort);
		}
	}
	
	// Update is called once per frame
	void Update () {
		sfs.ProcessEvents();
	}
	
	void OnApplicationQuit() {
		if (sfs.IsConnected) {
			DebugAdd ("Disconnecting from " + ServerIP + ":" + ServerPort);
			sfs.Disconnect();
		}
	}

	void DebugAdd (string txt) {
		debugNum++;
		debugText.text += "\n[" + debugNum + "] " + txt;
		if (addToDebugLog) Debug.Log (txt);
	}

}
